# code.py (LumosRing Clock example)
# Copyright: 2022 Bradan Lane STUDIO
#
# Open Source License: MIT

"""
This CircuitPython program demonstrates the basics of the LumosRing LEDs, buttons, and tone buzzer.

The LumosRing hardware connects via USB and provides:
    - 310 Neopixel LEDs
        - ring of 240 LEDs as 60 lines of 4 LEDs each
        - block of 10x7 LEDs as 7 lines of 10 LEDs each
    - tone buzzer
    - 2 buttons
    - a small mass storage device (used to store the code and related files)

The LumosRing is powered by a Lolin ESP32-S2 which include Wifi connectivity.

The sample code used the Adafruit IO API to synchronize a analog-style LED clock.
For a little fun, it animates a pair of eyes in the LumosRing block.

Documentation: 
**Hardware:**
    * the LumosRing (available on Tindie)
      https://www.tindie.com/stores/bradanlane/
"""

import supervisor
import random
import time

from LumosRing import LumosRing, SimpleTimer   # REQUIRED: import the LumosRing library

'''
try:
    from LumosRing import LumosRing,SimpleTimer   # REQUIRED: import the LumosRing library
except ImportError as e:
    print("------------------------------------------------------------------------")
    print(e)
    print("------------------------------------------------------------------------")
    print ("Error: Missing 'lib/LumosRing.py'")
    print ("The latest LumosRing library is available from:")
    print ("https://gitlab.com/bradanlane/circuitpython-projects/-/tree/main/LumosRing")
    print("------------------------------------------------------------------------")
'''

lumos = LumosRing(mode=LumosRing.NOCLOCK)
lumos.tone(440,0.5)

lumos.leds(LumosRing.WHITE)
lumos.clock_background = LumosRing.RED
lumos.letters("XK", LumosRing.WHITE, LumosRing.RED)

colors = (LumosRing.RED, LumosRing.GREEN, LumosRing.ORANGE, LumosRing.YELLOW, LumosRing.BLUE, LumosRing.MAGENTA, LumosRing.PURPLE, LumosRing.PINK, LumosRing.CYAN, LumosRing.WHITE, LumosRing.GRAY, LumosRing.DKGRAY, LumosRing.BLACK)
MAX_COLORS = 13
color = 0

timer = SimpleTimer(500)
timer.start()

counter = 0

while True:
    # we randomize eye movement (front is always between other positions)
    lumos.update()

    while lumos.buttons_changed:
        button, action = lumos.button
        if (button == LumosRing.RIGHT) and (action == LumosRing.PRESSED):
            lumos.tone(880,0.1)
            lumos.brightness += 0.05
            print("Right Button Increase Brightness to %f" % (lumos.brightness))
        if (button == LumosRing.LEFT) and (action == LumosRing.PRESSED):
            lumos.tone(220,0.1)
            lumos.brightness -= 0.05
            print("Left Button Decrease Brightness to %f" % (lumos.brightness))
    if timer.expired:
        timer.start()
        lumos.letters("--", colors[(color % MAX_COLORS)], colors[((color+1) % MAX_COLORS)])
        color = (color + 1) % MAX_COLORS
        counter = (counter + 1) % 4
        lumos.tone(110 * (counter + 1),0.1)
        print("Changing color to %d. New Tone %d. Restarting timer" % (color, (110* (counter+1))))

# end of while True loop
